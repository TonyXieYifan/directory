package edu.sjsu.android.project2tonyxie;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import java.util.ArrayList;

import edu.sjsu.android.project2tonyxie.databinding.ActivityMainBinding;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    public static ArrayList<Item> input = new ArrayList<>();
    public static int current;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = ActivityMainBinding.inflate(getLayoutInflater());
        View view = binding.getRoot();
        setContentView(view);

        input.add(new Item("Java", R.drawable.java, getString(R.string.description1)));
        input.add(new Item("C++", R.drawable.cpp, getString(R.string.description2)));
        input.add(new Item("Python", R.drawable.python, getString(R.string.description3)));
        input.add(new Item("Php", R.drawable.php, getString(R.string.description4)));
        input.add(new Item("Visual Basic", R.drawable.vb, getString(R.string.description5)));

        // Ensure each row has the same size
        binding.recycler.setHasFixedSize(true);
        // Set it as a list (linear layout)
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        binding.recycler.setLayoutManager(layoutManager);
        // Define and attach the recycler view to an adapter
        RecyclerView.Adapter<MyAdapter.ViewHolder> mAdapter = new MyAdapter(input);
        binding.recycler.setAdapter(mAdapter);

        binding.recycler.addOnItemTouchListener(new RecyclerItemClickListener(MainActivity.this,
                new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                if(position == 4){
                    AlertDialog dialog = new AlertDialog.Builder(MainActivity.this)
                            .setMessage("This programming language is unpopular. Are you sure you still want to know it?")
                            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    current = position;
                                    Intent intent = new Intent(view.getContext(), DetailActivity.class);
                                    intent.putExtra("current", input.get(position));
                                    startActivity(intent);
                                }
                            })
                            .setNegativeButton("No", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                }
                            })
                            .show();
                }
                else{
                    current = position;
                    Intent intent = new Intent(view.getContext(), DetailActivity.class);
                    intent.putExtra("current", input.get(position));
                    startActivity(intent);
                }
            }
        }));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu){
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.overflow_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem menuItem){
        int id = menuItem.getItemId();
        if(id == R.id.information){
            Intent intent = new Intent(MainActivity.this, InformationActivity.class);
            startActivity(intent);
        }
        else if (id == R.id.uninstall){
            Intent delete = new Intent(Intent.ACTION_DELETE, Uri.parse("package:" + getPackageName()));
            startActivity(delete);
        }

        return true;
    }
}