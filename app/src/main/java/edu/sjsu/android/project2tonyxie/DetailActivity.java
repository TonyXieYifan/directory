package edu.sjsu.android.project2tonyxie;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

public class DetailActivity extends MainActivity {

    private ImageView d_image;
    private TextView d_name;
    private TextView description;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_layout);
        d_image = findViewById(R.id.d_image);
        d_name = findViewById(R.id.d_name);
        description = findViewById(R.id.description);
        Item item = getIntent().getParcelableExtra("current");
        d_name.setText(item.getName());
        d_image.setImageResource(item.getImage());
        description.setText(item.getDescription());
    }

}
