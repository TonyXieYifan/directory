package edu.sjsu.android.project2tonyxie;

import android.content.Intent;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

import edu.sjsu.android.project2tonyxie.databinding.RowLayoutBinding;

import static androidx.core.content.ContextCompat.startActivity;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder>{
    private ArrayList<Item> data;

    public MyAdapter(ArrayList<Item> data){
        this.data = data;
    }

    @NonNull
    @Override
    public MyAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // Inflate (= create) a new view
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        RowLayoutBinding row = RowLayoutBinding.inflate(inflater);
        return new ViewHolder(row);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // get text from your data at this position
        final Item current = data.get(position);

        holder.binding.name.setText(current.getName());
        holder.binding.image.setImageResource(current.getImage());
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        protected final RowLayoutBinding binding;

        public ViewHolder(RowLayoutBinding binding) {
            super(binding.getRoot());
            this.binding = binding;
        }
    }


}
