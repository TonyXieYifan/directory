package edu.sjsu.android.project2tonyxie;

import android.os.Parcel;
import android.os.Parcelable;

public class Item implements Parcelable {
    private String name;
    private int image;
    private String description;

    public Item(String name, int image, String description){
        this.name = name;
        this.image = image;
        this.description = description;
    }

    protected Item(Parcel in) {
        name = in.readString();
        image = in.readInt();
        description = in.readString();
    }

    public static final Creator<Item> CREATOR = new Creator<Item>() {
        @Override
        public Item createFromParcel(Parcel in) {
            return new Item(in);
        }

        @Override
        public Item[] newArray(int size) {
            return new Item[size];
        }
    };

    public String getName() {
        return name;
    }

    public int getImage(){
        return image;
    }

    public String getDescription(){
        return description;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeInt(image);
        dest.writeString(description);
    }
}
